/*alert('Connected')*/


let studentList = []

function addStudent(name){
	studentList.push(name)
	return name + " was added to the student's list"
}

function countStudents(){
	return "There are a total of " + studentList.length + " enrolled."
}

function printStudents(){
	studentList.sort()
	studentList.forEach(function(student){
		console.log(student)
	})
}

function findStudent(searchedName){
	let filteredStudents = studentList.filter(function (student) {
		return student.toLowerCase() === searchedName.toLowerCase()
	}) 
	if (filteredStudents.length === 1) {
		console.log(filteredStudents[0] + ' is an enrollee.')
	} else if (filteredStudents.length > 1){
		console.log(filteredStudents.join() + ' are enrollees.')
	} else {
		console.log(searchedName + ' is not an enrollee.')
	}
} 

function addSection(section){
	let studentSection = studentList.map(function(student){
		return student + ' - section ' + section
 	})
 	console.log(studentSection)
}

function removeStudent(stuName) {
  return stuName.charAt(0).toUpperCase() + stuName.slice(1);
}

